# Zadanie1 

## John Carmack
![](https://upload.wikimedia.org/wikipedia/commons/4/4e/John_Carmack_at_GDCA_2017_--_1_March_2017_%28cropped%29.jpeg)

## Książki
1. "Paradyzja", Janusz Andrzej Zajdel
2. "1984", George Orwell
3. Saga "Harry Potter", J.K Rowling

## Miasta w Polsce
* Wrocław
* Włocławek
* Warszawa
* Konin
* Koszalin

## Najlepsze strony internetowe

* [baeldung.com](https://baeldung.com)
* [thispersondoesnotexist.com](https://thispersondoesnotexist.com/)

## Wyróżniamy wyrazy w zdaniu
Poprawność **dokumentu** proszę sprawdzić wykorzystując dedykowany edytor np. *Typora*. Jako wynik przesłać plik ~~zad1.m~~

## Latex w Markdown 
```math
 x^n + y^n = z^n 
```
## Poziom 2
No dzień dobry

### Poziom 3
No dzień dobry